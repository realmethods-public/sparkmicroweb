/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.delegate;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.*;

import com.occulue.primarykey.*;

import java.io.IOException;

import java.util.*;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Activity business delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of Activity related services in the case of a Activity business related service failing.</li>
 * <li>Exposes a simpler, uniform Activity interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill Activity business related services.</li>
 * </ol>
 * <p>
 * @author dev@realmethods.com
 */
public class ActivityBusinessDelegate extends BaseBusinessDelegate {
    // AIB : #getBusinessMethodImplementations( $classObject.getName() $classObject $classObject.getBusinessMethods() $classObject.getInterfaces() )
    // ~AIB

    //************************************************************************
    // Attributes
    //************************************************************************

    /**
     * Singleton instance
     */
    protected static ActivityBusinessDelegate singleton = null;
    private static final Logger LOGGER = Logger.getLogger(Activity.class.getName());

    //************************************************************************
    // Public Methods
    //************************************************************************
    /**
     * Default Constructor
     */
    public ActivityBusinessDelegate() {
    }

    /**
         * Activity Business Delegate Factory Method
         *
         * Returns a singleton instance of ActivityBusinessDelegate().
         * All methods are expected to be self-sufficient.
         *
         * @return         ActivityBusinessDelegate
         */
    public static ActivityBusinessDelegate getActivityInstance() {
        if (singleton == null) {
            singleton = new ActivityBusinessDelegate();
        }

        return (singleton);
    }

    /**
     * Method to retrieve the Activity via an ActivityPrimaryKey.
     * @param         key
     * @return         Activity
     * @exception ProcessingException - Thrown if processing any related problems
     * @exception IllegalArgumentException
     */
    public Activity getActivity(ActivityPrimaryKey key)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ActivityBusinessDelegate:getActivity - ";

        if (key == null) {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        Activity returnBO = null;

        ActivityDAO dao = getActivityDAO();

        try {
            returnBO = dao.findActivity(key);
        } catch (Exception exc) {
            String errMsg = "ActivityBusinessDelegate:getActivity( ActivityPrimaryKey key ) - unable to locate Activity with key " +
                key.toString() + " - " + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseActivityDAO(dao);
        }

        return returnBO;
    }

    /**
     * Method to retrieve a collection of all Activitys
     *
     * @return         ArrayList<Activity>
     * @exception ProcessingException Thrown if any problems
     */
    public ArrayList<Activity> getAllActivity() throws ProcessingException {
        String msgPrefix = "ActivityBusinessDelegate:getAllActivity() - ";
        ArrayList<Activity> array = null;

        ActivityDAO dao = getActivityDAO();

        try {
            array = dao.findAllActivity();
        } catch (Exception exc) {
            String errMsg = msgPrefix + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseActivityDAO(dao);
        }

        return array;
    }

    /**
     * Creates the provided BO.
     * @param                businessObject         Activity
     * @return       Activity
     * @exception    ProcessingException
     * @exception        IllegalArgumentException
     */
    public Activity createActivity(Activity businessObject)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ActivityBusinessDelegate:createActivity - ";

        if (businessObject == null) {
            String errMsg = msgPrefix + "null businessObject provided";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        // return value once persisted
        ActivityDAO dao = getActivityDAO();

        try {
            businessObject = dao.createActivity(businessObject);
        } catch (Exception exc) {
            String errMsg = "ActivityBusinessDelegate:createActivity() - Unable to create Activity" +
                exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseActivityDAO(dao);
        }

        return (businessObject);
    }

    /**
     * Saves the underlying BO.
     * @param                businessObject                Activity
     * @return       what was just saved
     * @exception    ProcessingException
     * @exception          IllegalArgumentException
     */
    public Activity saveActivity(Activity businessObject)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ActivityBusinessDelegate:saveActivity - ";

        if (businessObject == null) {
            String errMsg = msgPrefix + "null businessObject provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ActivityPrimaryKey key = businessObject.getActivityPrimaryKey();

        if (key != null) {
            ActivityDAO dao = getActivityDAO();

            try {
                businessObject = (Activity) dao.saveActivity(businessObject);
            } catch (Exception exc) {
                String errMsg = "ActivityBusinessDelegate:saveActivity() - Unable to save Activity" +
                    exc;
                LOGGER.warning(errMsg);
                throw new ProcessingException(errMsg);
            } finally {
                releaseActivityDAO(dao);
            }
        } else {
            String errMsg = "ActivityBusinessDelegate:saveActivity() - Unable to create Activity due to it having a null ActivityPrimaryKey.";

            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        }

        return (businessObject);
    }

    /**
     * Deletes the associatied value object using the provided primary key.
     * @param                key         ActivityPrimaryKey
     * @exception         ProcessingException
     */
    public void delete(ActivityPrimaryKey key)
        throws ProcessingException, IllegalArgumentException {
        String msgPrefix = "ActivityBusinessDelegate:saveActivity - ";

        if (key == null) {
            String errMsg = msgPrefix + "null key provided.";
            LOGGER.warning(errMsg);
            throw new IllegalArgumentException(errMsg);
        }

        ActivityDAO dao = getActivityDAO();

        try {
            dao.deleteActivity(key);
        } catch (Exception exc) {
            String errMsg = msgPrefix +
                "Unable to delete Activity using key = " + key + ". " + exc;
            LOGGER.warning(errMsg);
            throw new ProcessingException(errMsg);
        } finally {
            releaseActivityDAO(dao);
        }

        return;
    }

    // business methods
    /**
     * Returns the Activity specific DAO.
     *
     * @return      Activity DAO
     */
    public ActivityDAO getActivityDAO() {
        return (new com.occulue.dao.ActivityDAO());
    }

    /**
     * Release the ActivityDAO back to the FrameworkDAOFactory
     */
    public void releaseActivityDAO(com.occulue.dao.ActivityDAO dao) {
        dao = null;
    }
}

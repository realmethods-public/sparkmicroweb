/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity Admin.
 *
 * @author dev@realmethods.com
 */

// AIB : #getDAOClassDecl()
public class AdminDAO extends BaseDAO// ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(Admin.class.getName());

    /**
     * default constructor
     */
    public AdminDAO() {
    }

    /**
     * Retrieves a Admin from the persistent store, using the provided primary key.
     * If no match is found, a null Admin is returned.
     * <p>
     * @param       pk
     * @return      Admin
     * @exception   ProcessingException
     */
    public Admin findAdmin(AdminPrimaryKey pk) throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "AdminDAO.findAdmin(...) cannot have a null primary key argument");
        }

        Query query = null;
        Admin businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.Admin as admin where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("admin.adminId = " + pk.getAdminId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new Admin();
                businessObject.copy((Admin) query.list().iterator().next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "AdminDAO.findAdmin failed for primary key " + pk + " - " +
                exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AdminDAO.findAdmin - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all Admins
     * @return                ArrayList<Admin>
     * @exception   ProcessingException
     */
    public ArrayList<Admin> findAllAdmin() throws ProcessingException {
        ArrayList<Admin> list = new ArrayList<Admin>();
        ArrayList<Admin> refList = new ArrayList<Admin>();
        Query query = null;
        StringBuilder buf = new StringBuilder("from com.occulue.bo.Admin");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<Admin>) query.list();

                Admin tmp = null;

                for (Admin listEntry : list) {
                    tmp = new Admin();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException("AdminDAO.findAllAdmin failed - " +
                exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AdminDAO.findAllAdmin - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info("AdminDAO:findAllAdmins() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new Admin into the persistent store.
     * @param       businessObject
     * @return      newly persisted Admin
     * @exception   ProcessingException
     */
    public Admin createAdmin(Admin businessObject) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "AdminDAO.createAdmin - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("AdminDAO.createAdmin failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AdminDAO.createAdmin - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided Admin to the persistent store.
     *
     * @param       businessObject
     * @return      Admin        stored entity
     * @exception   ProcessingException
     */
    public Admin saveAdmin(Admin businessObject) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "AdminDAO.saveAdmin - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("AdminDAO.saveAdmin failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AdminDAO.saveAdmin - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a Admin from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteAdmin(AdminPrimaryKey pk) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            Admin bo = findAdmin(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "AdminDAO.deleteAdmin - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("AdminDAO.deleteAdmin failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "AdminDAO.deleteAdmin - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}

/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity ReferrerGroup.
 *
 * @author dev@realmethods.com
 */

// AIB : #getDAOClassDecl()
public class ReferrerGroupDAO extends BaseDAO// ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(ReferrerGroup.class.getName());

    /**
     * default constructor
     */
    public ReferrerGroupDAO() {
    }

    /**
     * Retrieves a ReferrerGroup from the persistent store, using the provided primary key.
     * If no match is found, a null ReferrerGroup is returned.
     * <p>
     * @param       pk
     * @return      ReferrerGroup
     * @exception   ProcessingException
     */
    public ReferrerGroup findReferrerGroup(ReferrerGroupPrimaryKey pk)
        throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "ReferrerGroupDAO.findReferrerGroup(...) cannot have a null primary key argument");
        }

        Query query = null;
        ReferrerGroup businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.ReferrerGroup as referrergroup where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("referrergroup.referrerGroupId = " +
                pk.getReferrerGroupId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new ReferrerGroup();
                businessObject.copy((ReferrerGroup) query.list().iterator()
                                                         .next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "ReferrerGroupDAO.findReferrerGroup failed for primary key " +
                pk + " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferrerGroupDAO.findReferrerGroup - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all ReferrerGroups
     * @return                ArrayList<ReferrerGroup>
     * @exception   ProcessingException
     */
    public ArrayList<ReferrerGroup> findAllReferrerGroup()
        throws ProcessingException {
        ArrayList<ReferrerGroup> list = new ArrayList<ReferrerGroup>();
        ArrayList<ReferrerGroup> refList = new ArrayList<ReferrerGroup>();
        Query query = null;
        StringBuilder buf = new StringBuilder(
                "from com.occulue.bo.ReferrerGroup");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<ReferrerGroup>) query.list();

                ReferrerGroup tmp = null;

                for (ReferrerGroup listEntry : list) {
                    tmp = new ReferrerGroup();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException(
                "ReferrerGroupDAO.findAllReferrerGroup failed - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferrerGroupDAO.findAllReferrerGroup - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info(
                "ReferrerGroupDAO:findAllReferrerGroups() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new ReferrerGroup into the persistent store.
     * @param       businessObject
     * @return      newly persisted ReferrerGroup
     * @exception   ProcessingException
     */
    public ReferrerGroup createReferrerGroup(ReferrerGroup businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferrerGroupDAO.createReferrerGroup - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferrerGroupDAO.createReferrerGroup failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferrerGroupDAO.createReferrerGroup - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided ReferrerGroup to the persistent store.
     *
     * @param       businessObject
     * @return      ReferrerGroup        stored entity
     * @exception   ProcessingException
     */
    public ReferrerGroup saveReferrerGroup(ReferrerGroup businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferrerGroupDAO.saveReferrerGroup - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferrerGroupDAO.saveReferrerGroup failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferrerGroupDAO.saveReferrerGroup - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a ReferrerGroup from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteReferrerGroup(ReferrerGroupPrimaryKey pk)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            ReferrerGroup bo = findReferrerGroup(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferrerGroupDAO.deleteReferrerGroup - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferrerGroupDAO.deleteReferrerGroup failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferrerGroupDAO.deleteReferrerGroup - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}

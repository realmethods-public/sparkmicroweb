/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity TheReference.
 *
 * @author dev@realmethods.com
 */

// AIB : #getDAOClassDecl()
public class TheReferenceDAO extends BaseDAO// ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(TheReference.class.getName());

    /**
     * default constructor
     */
    public TheReferenceDAO() {
    }

    /**
     * Retrieves a TheReference from the persistent store, using the provided primary key.
     * If no match is found, a null TheReference is returned.
     * <p>
     * @param       pk
     * @return      TheReference
     * @exception   ProcessingException
     */
    public TheReference findTheReference(TheReferencePrimaryKey pk)
        throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "TheReferenceDAO.findTheReference(...) cannot have a null primary key argument");
        }

        Query query = null;
        TheReference businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.TheReference as thereference where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("thereference.theReferenceId = " +
                pk.getTheReferenceId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new TheReference();
                businessObject.copy((TheReference) query.list().iterator().next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "TheReferenceDAO.findTheReference failed for primary key " +
                pk + " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "TheReferenceDAO.findTheReference - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all TheReferences
     * @return                ArrayList<TheReference>
     * @exception   ProcessingException
     */
    public ArrayList<TheReference> findAllTheReference()
        throws ProcessingException {
        ArrayList<TheReference> list = new ArrayList<TheReference>();
        ArrayList<TheReference> refList = new ArrayList<TheReference>();
        Query query = null;
        StringBuilder buf = new StringBuilder(
                "from com.occulue.bo.TheReference");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<TheReference>) query.list();

                TheReference tmp = null;

                for (TheReference listEntry : list) {
                    tmp = new TheReference();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException(
                "TheReferenceDAO.findAllTheReference failed - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "TheReferenceDAO.findAllTheReference - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info(
                "TheReferenceDAO:findAllTheReferences() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new TheReference into the persistent store.
     * @param       businessObject
     * @return      newly persisted TheReference
     * @exception   ProcessingException
     */
    public TheReference createTheReference(TheReference businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "TheReferenceDAO.createTheReference - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "TheReferenceDAO.createTheReference failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "TheReferenceDAO.createTheReference - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided TheReference to the persistent store.
     *
     * @param       businessObject
     * @return      TheReference        stored entity
     * @exception   ProcessingException
     */
    public TheReference saveTheReference(TheReference businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "TheReferenceDAO.saveTheReference - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "TheReferenceDAO.saveTheReference failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "TheReferenceDAO.saveTheReference - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a TheReference from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteTheReference(TheReferencePrimaryKey pk)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            TheReference bo = findTheReference(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "TheReferenceDAO.deleteTheReference - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "TheReferenceDAO.deleteTheReference failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "TheReferenceDAO.deleteTheReference - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}

/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity Question.
 *
 * @author dev@realmethods.com
 */

// AIB : #getDAOClassDecl()
public class QuestionDAO extends BaseDAO// ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(Question.class.getName());

    /**
     * default constructor
     */
    public QuestionDAO() {
    }

    /**
     * Retrieves a Question from the persistent store, using the provided primary key.
     * If no match is found, a null Question is returned.
     * <p>
     * @param       pk
     * @return      Question
     * @exception   ProcessingException
     */
    public Question findQuestion(QuestionPrimaryKey pk)
        throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "QuestionDAO.findQuestion(...) cannot have a null primary key argument");
        }

        Query query = null;
        Question businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.Question as question where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("question.questionId = " +
                pk.getQuestionId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new Question();
                businessObject.copy((Question) query.list().iterator().next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "QuestionDAO.findQuestion failed for primary key " + pk +
                " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "QuestionDAO.findQuestion - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all Questions
     * @return                ArrayList<Question>
     * @exception   ProcessingException
     */
    public ArrayList<Question> findAllQuestion() throws ProcessingException {
        ArrayList<Question> list = new ArrayList<Question>();
        ArrayList<Question> refList = new ArrayList<Question>();
        Query query = null;
        StringBuilder buf = new StringBuilder("from com.occulue.bo.Question");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<Question>) query.list();

                Question tmp = null;

                for (Question listEntry : list) {
                    tmp = new Question();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException(
                "QuestionDAO.findAllQuestion failed - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "QuestionDAO.findAllQuestion - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info("QuestionDAO:findAllQuestions() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new Question into the persistent store.
     * @param       businessObject
     * @return      newly persisted Question
     * @exception   ProcessingException
     */
    public Question createQuestion(Question businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "QuestionDAO.createQuestion - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "QuestionDAO.createQuestion failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "QuestionDAO.createQuestion - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided Question to the persistent store.
     *
     * @param       businessObject
     * @return      Question        stored entity
     * @exception   ProcessingException
     */
    public Question saveQuestion(Question businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "QuestionDAO.saveQuestion - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("QuestionDAO.saveQuestion failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "QuestionDAO.saveQuestion - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a Question from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteQuestion(QuestionPrimaryKey pk)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            Question bo = findQuestion(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "QuestionDAO.deleteQuestion - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "QuestionDAO.deleteQuestion failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "QuestionDAO.deleteQuestion - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}

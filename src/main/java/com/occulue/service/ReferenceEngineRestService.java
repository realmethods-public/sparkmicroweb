/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.occulue.bo.*;
import com.occulue.bo.Base;

import com.occulue.common.JsonTransformer;

import com.occulue.delegate.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import spark.Request;
import spark.Response;
import spark.Route;
import static spark.Spark.get;
import static spark.Spark.post;

import java.io.IOException;
import java.io.StringWriter;

import java.text.SimpleDateFormat;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;


/**
 * Implements Struts action processing for business entity ReferenceEngine.
 *
 * @author dev@realmethods.com
 */
public class ReferenceEngineRestService extends BaseRestService {
    private static final Logger LOGGER = Logger.getLogger(BaseRestService.class.getName());

    //************************************************************************    
    // Attributes
    //************************************************************************
    private ReferenceEngine referenceEngine = null;

    public ReferenceEngineRestService() {
    }

    /**
     * Handles saving a ReferenceEngine BO.  if not key provided, calls create, otherwise calls save
     * @exception        ProcessingException
     */
    protected ReferenceEngine save() throws ProcessingException {
        // doing it here helps
        getReferenceEngine();

        LOGGER.info("ReferenceEngine.save() on - " + referenceEngine);

        if (hasPrimaryKey()) {
            return (update());
        } else {
            return (create());
        }
    }

    /**
     * Returns true if the referenceEngine is non-null and has it's primary key field(s) set
     * @return                boolean
     */
    protected boolean hasPrimaryKey() {
        boolean hasPK = false;

        if ((referenceEngine != null) &&
                (referenceEngine.getReferenceEnginePrimaryKey().hasBeenAssigned() == true)) {
            hasPK = true;
        }

        return (hasPK);
    }

    /**
     * Handles updating a ReferenceEngine BO
     * @return                ReferenceEngine
     * @exception        ProcessingException
     */
    protected ReferenceEngine update() throws ProcessingException {
        // store provided data
        ReferenceEngine tmp = referenceEngine;

        // load actual data from storage
        loadHelper(referenceEngine.getReferenceEnginePrimaryKey());

        // copy provided data into actual data
        referenceEngine.copyShallow(tmp);

        try {
            // create the ReferenceEngineBusiness Delegate            
            ReferenceEngineBusinessDelegate delegate = ReferenceEngineBusinessDelegate.getReferenceEngineInstance();
            this.referenceEngine = delegate.saveReferenceEngine(referenceEngine);

            if (this.referenceEngine != null) {
                LOGGER.info(
                    "ReferenceEngineRestService:update() - successfully updated ReferenceEngine - " +
                    referenceEngine.toString());
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ReferenceEngineRestService:update() - successfully update ReferenceEngine - " +
                exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return this.referenceEngine;
    }

    /**
     * Handles creating a ReferenceEngine BO
     * @return                ReferenceEngine
     */
    protected ReferenceEngine create() throws ProcessingException {
        try {
            referenceEngine = getReferenceEngine();
            this.referenceEngine = ReferenceEngineBusinessDelegate.getReferenceEngineInstance()
                                                                  .createReferenceEngine(referenceEngine);
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ReferenceEngineRestService:create() - exception ReferenceEngine - " +
                exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return this.referenceEngine;
    }

    /**
     * Handles deleting a ReferenceEngine BO
     * @exception        ProcessingException
     */
    protected void delete() throws ProcessingException {
        try {
            ReferenceEngineBusinessDelegate delegate = ReferenceEngineBusinessDelegate.getReferenceEngineInstance();

            Long[] childIds = getChildIds();

            if ((childIds == null) || (childIds.length == 0)) {
                Long referenceEngineId = parseId("referenceEngineId");
                delegate.delete(new ReferenceEnginePrimaryKey(referenceEngineId));
                LOGGER.info(
                    "ReferenceEngineRestService:delete() - successfully deleted ReferenceEngine with key " +
                    referenceEngine.getReferenceEnginePrimaryKey()
                                   .valuesAsCollection());
            } else {
                for (Long id : childIds) {
                    try {
                        delegate.delete(new ReferenceEnginePrimaryKey(id));
                    } catch (Throwable exc) {
                        signalBadRequest();

                        String errMsg = "ReferenceEngineRestService:delete() - " +
                            exc.getMessage();
                        LOGGER.severe(errMsg);
                        throw new ProcessingException(errMsg);
                    }
                }
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ReferenceEngineRestService:delete() - " +
                exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }
    }

    /**
     * Handles loading a ReferenceEngine BO
     * @param                Long referenceEngineId
     * @exception        ProcessingException
     * @return                ReferenceEngine
     */
    protected ReferenceEngine load() throws ProcessingException {
        ReferenceEnginePrimaryKey pk = null;
        Long referenceEngineId = parseId("referenceEngineId");

        try {
            LOGGER.info("ReferenceEngine.load pk is " + referenceEngineId);

            if (referenceEngineId != null) {
                pk = new ReferenceEnginePrimaryKey(referenceEngineId);

                loadHelper(pk);

                // load the contained instance of ReferenceEngine
                this.referenceEngine = ReferenceEngineBusinessDelegate.getReferenceEngineInstance()
                                                                      .getReferenceEngine(pk);

                LOGGER.info(
                    "ReferenceEngineRestService:load() - successfully loaded - " +
                    this.referenceEngine.toString());
            } else {
                signalBadRequest();

                String errMsg = "ReferenceEngineRestService:load() - unable to locate the primary key as an attribute or a selection for - " +
                    referenceEngine.toString();
                LOGGER.severe(errMsg);
                throw new ProcessingException(errMsg);
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ReferenceEngineRestService:load() - failed to load ReferenceEngine using Id " +
                referenceEngineId + ", " + exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return referenceEngine;
    }

    /**
     * Handles loading all ReferenceEngine business objects
     * @return                List<ReferenceEngine>
     * @exception        ProcessingException
     */
    protected List<ReferenceEngine> loadAll() throws ProcessingException {
        List<ReferenceEngine> referenceEngineList = null;

        try {
            // load the ReferenceEngine
            referenceEngineList = ReferenceEngineBusinessDelegate.getReferenceEngineInstance()
                                                                 .getAllReferenceEngine();

            if (referenceEngineList != null) {
                LOGGER.info(
                    "ReferenceEngineRestService:loadAllReferenceEngine() - successfully loaded all ReferenceEngines");
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ReferenceEngineRestService:loadAll() - failed to load all ReferenceEngines - " +
                exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return referenceEngineList;
    }

    /**
     * save MainQuestionGroup on ReferenceEngine
     * @param                ReferenceEngine referenceEngine
     * @return                ReferenceEngine
     * @exception        ProcessingException
     */
    protected ReferenceEngine saveMainQuestionGroup()
        throws ProcessingException {
        Long referenceEngineId = parseId("referenceEngineId");
        Long childId = parseId("childId");

        if (loadHelper(new ReferenceEnginePrimaryKey(referenceEngineId)) == null) {
            return (null);
        }

        if (childId != null) {
            QuestionGroupBusinessDelegate childDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
            ReferenceEngineBusinessDelegate parentDelegate = ReferenceEngineBusinessDelegate.getReferenceEngineInstance();
            QuestionGroup child = null;

            try {
                child = childDelegate.getQuestionGroup(new QuestionGroupPrimaryKey(
                            childId));
            } catch (Throwable exc) {
                signalBadRequest();

                String errMsg = "ReferenceEngineRestService:saveMainQuestionGroup() failed to get QuestionGroup using id " +
                    childId + " - " + exc.getMessage();
                LOGGER.severe(errMsg);
                throw new ProcessingException(errMsg);
            }

            referenceEngine.setMainQuestionGroup(child);

            try {
                // save it
                parentDelegate.saveReferenceEngine(referenceEngine);
            } catch (Exception exc) {
                signalBadRequest();

                String errMsg = "ReferenceEngineRestService:saveMainQuestionGroup() failed saving parent ReferenceEngine - " +
                    exc.getMessage();
                LOGGER.severe(errMsg);
                throw new ProcessingException(errMsg);
            }
        }

        return referenceEngine;
    }

    /**
     * delete MainQuestionGroup on ReferenceEngine
     * @return                ReferenceEngine
     * @exception        ProcessingException
     */
    protected ReferenceEngine deleteMainQuestionGroup()
        throws ProcessingException {
        Long referenceEngineId = parseId("referenceEngineId");

        if (loadHelper(new ReferenceEnginePrimaryKey(referenceEngineId)) == null) {
            return (null);
        }

        if (referenceEngine.getMainQuestionGroup() != null) {
            QuestionGroupPrimaryKey pk = referenceEngine.getMainQuestionGroup()
                                                        .getQuestionGroupPrimaryKey();

            // null out the parent first so there's no constraint during deletion
            referenceEngine.setMainQuestionGroup(null);

            try {
                ReferenceEngineBusinessDelegate parentDelegate = ReferenceEngineBusinessDelegate.getReferenceEngineInstance();

                // save it
                referenceEngine = parentDelegate.saveReferenceEngine(referenceEngine);
            } catch (Exception exc) {
                signalBadRequest();

                String errMsg = "ReferenceEngineRestService:deleteMainQuestionGroup() failed to save ReferenceEngine - " +
                    exc.getMessage();
                LOGGER.severe(errMsg);
                throw new ProcessingException(errMsg);
            }

            try {
                // safe to delete the child			
                QuestionGroupBusinessDelegate childDelegate = QuestionGroupBusinessDelegate.getQuestionGroupInstance();
                childDelegate.delete(pk);
            } catch (Exception exc) {
                signalBadRequest();

                String errMsg = "ReferenceEngineRestService:deleteMainQuestionGroup() failed  - " +
                    exc.getMessage();
                LOGGER.severe(errMsg);
                throw new ProcessingException(errMsg);
            }
        }

        return referenceEngine;
    }

    protected ReferenceEngine loadHelper(ReferenceEnginePrimaryKey pk)
        throws ProcessingException {
        try {
            LOGGER.info("ReferenceEngine.loadHelper primary key is " + pk);

            if (pk != null) {
                // load the contained instance of ReferenceEngine
                this.referenceEngine = ReferenceEngineBusinessDelegate.getReferenceEngineInstance()
                                                                      .getReferenceEngine(pk);

                LOGGER.info(
                    "ReferenceEngineRestService:loadHelper() - successfully loaded - " +
                    this.referenceEngine.toString());
            } else {
                signalBadRequest();

                String errMsg = "ReferenceEngineRestService:loadHelper() - null primary key provided.";
                LOGGER.severe(errMsg);
                throw new ProcessingException(errMsg);
            }
        } catch (Throwable exc) {
            signalBadRequest();

            String errMsg = "ReferenceEngineRestService:load() - failed to load ReferenceEngine using pk " +
                pk + ", " + exc.getMessage();
            LOGGER.severe(errMsg);
            throw new ProcessingException(errMsg);
        }

        return referenceEngine;
    }

    // overloads from BaseRestService

    /**
     * main handler for execution
     * @param action
     * @param response
     * @param request
     * @return
     * @throws ProcessingException
     */
    public Object handleExec(String action, spark.Response response,
        spark.Request request) throws ProcessingException {
        // store locally
        this.response = response;
        this.request = request;

        if (action == null) {
            signalBadRequest();
            throw new ProcessingException();
        }

        Object returnVal = null;

        switch (action) {
        case "save":
            returnVal = save();

            break;

        case "load":
            returnVal = load();

            break;

        case "delete":
            delete();

            break;

        case "loadAll":
        case "viewAll":
            returnVal = loadAll();

            break;

        case "saveMainQuestionGroup":
            returnVal = saveMainQuestionGroup().getMainQuestionGroup();

            break;

        case "deleteMainQuestionGroup":
            returnVal = deleteMainQuestionGroup().getMainQuestionGroup();

            break;

        case "loadMainQuestionGroup":
            returnVal = load().getMainQuestionGroup();

            break;

        default:
            signalBadRequest();
            throw new ProcessingException(
                "ReferenceEngine.execute(...) - unable to handle action " +
                action);
        }

        return returnVal;
    }

    /**
     * Uses ObjectMapper to map from Json to a ReferenceEngine. Found in the request body.
     *
     * @return ReferenceEngine
     */
    private ReferenceEngine getReferenceEngine() {
        if (referenceEngine == null) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
                referenceEngine = mapper.readValue(java.net.URLDecoder.decode(
                            request.queryString(), "UTF-8"),
                        ReferenceEngine.class);
            } catch (Exception exc) {
                signalBadRequest();
                LOGGER.severe(
                    "ReferenceEngineRestService.getReferenceEngine() - failed to Json map from String to ReferenceEngine - " +
                    exc.getMessage());
            }
        }

        return (referenceEngine);
    }

    protected String getSubclassName() {
        return ("ReferenceEngineRestService");
    }
}
